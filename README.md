# How to run
Run `java -jar api-places-0.0.1.jar`

------------------------------------------------

# How to call rest
Inside the dir have a json of postman with the calls,
just import json on postman

### FindBySlug
`curl -X GET 'http://localhost:8081/api-places/places/rio-de-janeiro-rj-monte-azula' -i`
  
### FindById
`curl -X GET 'http://localhost:8081/api-places/places/9' -i`

### Create a place
`curl -X POST 'http://localhost:8081/api-places/places' -H 'Content-Type: application/json' -d '{ "name": "São Paulo, SP - Limão, 123", "city": "São Paulo" }' -i`

### Edit a place
`curl -X PUT 'http://localhost:8081/api-places/places' -H 'Content-Type: application/json' -d '{ "id": 9, "name": "Rio de Janeiro, RJ - Monte Azuleta", "city": "Rio de Janeiro" }' -i`

### List all pagable
`curl -X GET 'http://localhost:8081/api-places/places/list?pages=0&size=2' -i`

### Filter By Name
`curl -X GET 'http://localhost:8081/api-places/places?name=paulo' -i`

