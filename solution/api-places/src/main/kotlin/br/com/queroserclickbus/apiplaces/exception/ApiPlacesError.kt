package br.com.queroserclickbus.apiplaces.exception

class ApiPlacesError(errorCode: String, var errorMessage: String) {

    private val appPrefix = "PLACE"
    var errorCode: String? = null
        get() = appPrefix + field

    init {
        this.errorCode = errorCode
    }

    companion object {
        val PLACE_ALREADY_EXISTS = ApiPlacesError("001", "Place has already been created")
    }
}

