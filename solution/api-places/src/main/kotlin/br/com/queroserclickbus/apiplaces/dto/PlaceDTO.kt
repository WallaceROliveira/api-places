package br.com.queroserclickbus.apiplaces.dto

import java.io.Serializable
import java.util.*

data class PlaceDTO(val id: Long? = null,
                    val name: String? = null,
                    var slug: String? = null,
                    val city: String? = null,
                    val createdAt: Date? = null,
                    val updatedAt: Date? = null) : Serializable
