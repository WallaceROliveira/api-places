package br.com.queroserclickbus.apiplaces.service

import br.com.queroserclickbus.apiplaces.dto.PlaceDTO
import br.com.queroserclickbus.apiplaces.model.Place
import org.springframework.data.domain.Page

interface PlaceService {

    fun findById(id: Long): PlaceDTO

    fun save(placeDTO: PlaceDTO): PlaceDTO

    fun update(placeDTO: PlaceDTO): PlaceDTO?

    fun findBySlug(slug: String): PlaceDTO

    fun findByIdOrSlug(field: String): PlaceDTO

    fun filterByName(name: String): List<PlaceDTO>

    fun findAll(quantity: Int, pages: Int): Page<Place>

}
