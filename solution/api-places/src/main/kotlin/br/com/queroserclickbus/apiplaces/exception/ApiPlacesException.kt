package br.com.queroserclickbus.apiplaces.exception


class ApiPlacesException(val apiPlacesError: ApiPlacesError) : Exception(apiPlacesError.errorMessage)
