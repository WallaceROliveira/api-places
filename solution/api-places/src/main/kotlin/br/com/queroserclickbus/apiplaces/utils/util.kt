package br.com.queroserclickbus.apiplaces.utils

import java.text.Normalizer
import java.util.*
import java.util.regex.Pattern


fun toSlug(input: String): String {
    val dash = Pattern.compile("[\\-]").matcher(input).replaceAll(" ")
    val nowhitespace = Pattern.compile("[\\s]+").matcher(dash).replaceAll("-")
    val normalized = Normalizer.normalize(nowhitespace, Normalizer.Form.NFD)
    val slug = Pattern.compile("[^\\w-]").matcher(normalized).replaceAll("")
    return slug.toLowerCase(Locale.ENGLISH)
}

fun returnLongIfStringContainOnlyNumber(field: String): Any =
        if(field.matches(Regex("^[0-9]*\$"))) field.toLong() else field

