package br.com.queroserclickbus.apiplaces.model

import org.jetbrains.annotations.NotNull
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "place")
data class Place(@Id
                 @GeneratedValue(strategy = GenerationType.IDENTITY)
                 val id: Long? = null,

                 @NotNull
                 @Column(name = "name")
                 var name: String,

                 @NotNull
                 @Column(name = "slug", unique = true)
                 var slug: String? = null,

                 @NotNull
                 @Column(name = "city")
                 var city: String,

                 @NotNull
                 @Temporal(TemporalType.TIMESTAMP)
                 @Column(name = "created_at")
                 val createdAt: Date? = Date(),

                 @Column(name = "updated_at")
                 @Temporal(TemporalType.TIMESTAMP)
                 var updatedAt: Date? = null)
