package br.com.queroserclickbus.apiplaces.config

import org.modelmapper.ModelMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class BeansConfiguration {

    @Bean
    fun modelMapper(): ModelMapper =
            ModelMapper().apply {
                getConfiguration()
                        .setFieldAccessLevel(org.modelmapper.config.Configuration.AccessLevel.PRIVATE)
                        .setFieldMatchingEnabled(true)
            }


}
