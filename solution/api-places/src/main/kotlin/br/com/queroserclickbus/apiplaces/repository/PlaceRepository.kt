package br.com.queroserclickbus.apiplaces.repository

import br.com.queroserclickbus.apiplaces.model.Place
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository


@Repository
interface PlaceRepository : JpaRepository<Place, Long> {

    fun existsBySlug(slug: String): Boolean
    fun findBySlug(slug: String): Place?

    @Query("FROM Place p WHERE upper(p.name)" + " LIKE concat('%', upper(:name), '%')")
    fun filterByName(@Param("name") nome: String): List<Place>

    override fun findAll(pageable: Pageable): Page<Place>

}
