package br.com.queroserclickbus.apiplaces.controller

import br.com.queroserclickbus.apiplaces.dto.PlaceDTO
import br.com.queroserclickbus.apiplaces.exception.ApiPlacesException
import br.com.queroserclickbus.apiplaces.model.Place
import br.com.queroserclickbus.apiplaces.service.PlaceServiceImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.persistence.NoResultException

@RestController
@RequestMapping("places")
class PlaceController(@Autowired val placeServiceImpl: PlaceServiceImpl) {

    @PostMapping
    fun save(@RequestBody placeDTO: PlaceDTO): ResponseEntity<PlaceDTO> {
        return try {
            ResponseEntity.ok(placeServiceImpl.save(placeDTO))
        } catch (e: ApiPlacesException) {
            ResponseEntity.status(HttpStatus.CONFLICT).build()
        }
    }

    @PutMapping
    fun update(@RequestBody placeDTO: PlaceDTO): ResponseEntity<PlaceDTO?> {
        return try {
            ResponseEntity.ok(placeServiceImpl.update(placeDTO)!!)
        } catch (e: NoResultException) {
            ResponseEntity.noContent().build()
        } catch (e: ApiPlacesException) {
            ResponseEntity.status(HttpStatus.CONFLICT).build()
        }

    }

    @GetMapping("{field}")
    fun getByIdOrSlug(@PathVariable field: String): ResponseEntity<PlaceDTO> {
        return try {
            ResponseEntity.ok(placeServiceImpl.findByIdOrSlug(field))
        } catch (e: NoResultException) {
            ResponseEntity.noContent().build()
        }
    }

    @GetMapping
    fun getByName(@RequestParam name: String): ResponseEntity<List<PlaceDTO>> =
            ResponseEntity.ok(placeServiceImpl.filterByName(name))

    @GetMapping("list")
    fun getAll(@RequestParam pages:Int, @RequestParam size: Int): ResponseEntity<Page<Place>> =
            ResponseEntity.ok(placeServiceImpl.findAll(pages, size))

}
