package br.com.queroserclickbus.apiplaces

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ApiPlacesApplication

fun main(args: Array<String>) {
	runApplication<ApiPlacesApplication>(*args)
}
