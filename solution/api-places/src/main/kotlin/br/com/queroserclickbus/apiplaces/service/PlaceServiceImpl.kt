package br.com.queroserclickbus.apiplaces.service

import br.com.queroserclickbus.apiplaces.dto.PlaceDTO
import br.com.queroserclickbus.apiplaces.exception.ApiPlacesError
import br.com.queroserclickbus.apiplaces.exception.ApiPlacesException
import br.com.queroserclickbus.apiplaces.model.Place
import br.com.queroserclickbus.apiplaces.repository.PlaceRepository
import br.com.queroserclickbus.apiplaces.utils.returnLongIfStringContainOnlyNumber
import br.com.queroserclickbus.apiplaces.utils.toSlug
import org.modelmapper.ModelMapper
import org.modelmapper.TypeToken
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.stereotype.Service
import java.util.*
import javax.persistence.NoResultException

@Service
class PlaceServiceImpl(@Autowired val placeRepository: PlaceRepository, @Autowired val modelMapper: ModelMapper) : PlaceService {

    override fun save(placeDTO: PlaceDTO): PlaceDTO {
        if (placeDTO.name.isNullOrBlank() || placeDTO.city.isNullOrBlank()) throw IllegalArgumentException("Name and City cannot be empty or null")
        else placeDTO.slug = toSlug(placeDTO.name)

        if (!placeRepository.existsBySlug(placeDTO.slug!!)) {
            var place = Place(name = placeDTO.name, slug = placeDTO.slug!!, city = placeDTO.city!!)
            placeRepository.save(place)
            return modelMapper.map(place, PlaceDTO::class.java)
        } else {
            throw ApiPlacesException(ApiPlacesError.PLACE_ALREADY_EXISTS)
        }
    }

    override fun findByIdOrSlug(field: String): PlaceDTO =
            when (val fieldValidated = returnLongIfStringContainOnlyNumber(field)) {
                is Long -> findById(fieldValidated)
                is String -> findBySlug(fieldValidated)
                else -> throw java.lang.IllegalArgumentException("Invalid format field")
            }

    override fun findById(id: Long): PlaceDTO {
        val placeOptional = placeRepository.findById(id)
        if (placeOptional.isPresent) {
            var place = placeOptional.get()
            return modelMapper.map(place, PlaceDTO::class.java)
        } else {
            throw NoResultException("Place not exists")
        }
    }

    override fun update(placeDTO: PlaceDTO): PlaceDTO? {
        if (placeDTO.name.isNullOrBlank() || placeDTO.city.isNullOrBlank()) throw IllegalArgumentException("Name and City cannot be empty or null")

        val placeOptional = placeRepository.findById(placeDTO.id!!)
        if (placeOptional.isPresent) {
            val slug = toSlug(placeDTO.name)
            if (placeRepository.existsBySlug(slug)) throw ApiPlacesException(ApiPlacesError.PLACE_ALREADY_EXISTS)

            val place = placeOptional.get()
            place.name = placeDTO.name
            place.city = placeDTO.city
            place.slug = slug
            place.updatedAt = Date()

            val placeUpdated = placeRepository.save(place)

            return modelMapper.map(placeUpdated, PlaceDTO::class.java)
        } else {
            throw NoResultException("Place not exists")
        }

    }

    override fun findBySlug(slug: String): PlaceDTO {
        val place = placeRepository.findBySlug(slug)
        if (place != null) {
            return modelMapper.map(place, PlaceDTO::class.java)
        } else {
            throw NoResultException("Place not exists")
        }
    }

    override fun filterByName(name: String): List<PlaceDTO> =
            modelMapper.map(placeRepository.filterByName(name), object : TypeToken<List<PlaceDTO>>() {}.type)


    override fun findAll(pages: Int, size: Int): Page<Place> =
            placeRepository.findAll(pageable = PageRequest.of(pages, size))


}
