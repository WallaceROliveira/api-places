package br.com.queroserclickbus.apiplaces.model

import br.com.queroserclickbus.apiplaces.utils.toSlug
import br.com.six2six.fixturefactory.Fixture
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader
import org.junit.Assert.*
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
class PlaceTest {

    companion object {
        @BeforeClass
        @JvmStatic
        fun setUp() =
                FixtureFactoryLoader.loadTemplates("br.com.queroserclickbus.apiplaces.fixture")
    }

    @Test
    fun validPlace() {
        val place = Fixture.from(Place::class.java).gimme<Place>("validPlace")
        assertNotNull("Place should not be null", place)
        assertNotNull("Name should not be null", place.name)
        assertNotNull("City should not be null", place.city)
        assertNotNull("Slug should not be null", place.slug)
        assertNotNull("CreatedAt should not be null", place.createdAt)
    }

    @Test
    fun validPlaceList() {
        val places = Fixture.from(Place::class.java).gimme<Place>(5,"validPlace")
        assertNotNull("Places list should not be null", places)
		assertFalse("Places list should not be empty", places.isEmpty())
		assertEquals("Places list size should be 5", places.size, 5)

        for (place in places){
            assertNotNull("Place should not be null", place)
            assertNotNull("Name should not be null", place.name)
            assertNotNull("City should not be null", place.city)
            assertNotNull("Slug should not be null", place.slug)
            assertNotNull("CreatedAt should not be null", place.createdAt)
        }
    }

    @Test
    fun create_slug(){
        val place = Fixture.from(Place::class.java).gimme<Place>("validPlaceWithoutSlug")
        place.slug = toSlug(place.name)
        val expectedSlugName = "jd-paulista"
        assertNotNull("Slug should not be null", place.slug)
        assertEquals(expectedSlugName, place.slug)
    }

}
