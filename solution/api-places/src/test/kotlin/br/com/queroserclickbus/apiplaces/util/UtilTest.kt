package br.com.queroserclickbus.apiplaces.util

import br.com.queroserclickbus.apiplaces.utils.returnLongIfStringContainOnlyNumber
import br.com.queroserclickbus.apiplaces.utils.toSlug
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class UtilTest {

    @Test
    fun pass_string_without_number_return_string_type() {
        val string = "sao-paulo-tiete"
        val fieldString = returnLongIfStringContainOnlyNumber(string)
        Assert.assertEquals(String::class, fieldString::class)
    }

    @Test
    fun pass_string_with_number_return_string_type() {
        val stringWithNumber = "sao-paulo-tiete123123"
        val fieldString = returnLongIfStringContainOnlyNumber(stringWithNumber)
        Assert.assertEquals(String::class, fieldString::class)
    }

    @Test
    fun pass_string_with_only_number_return_long_type() {
        val stringWithOnlyNumber = "1233"
        val fieldLong = returnLongIfStringContainOnlyNumber(stringWithOnlyNumber)
        Assert.assertEquals(Long::class, fieldLong::class)
    }

    @Test
    fun create_slug_by_name_with_dash_whitespace_comma() {
        val name = "São Paulo, SP - Tiete"
        val expectedSlugName = "sao-paulo-sp-tiete"
        val slugName = toSlug(name)
        Assert.assertEquals(expectedSlugName, slugName)
    }

    @Test
    fun create_slug_by_name_with_dash_whitespace() {
        val name = "São Paulo SP - Tiete"
        val expectedSlugName = "sao-paulo-sp-tiete"
        val slugName = toSlug(name)
        Assert.assertEquals(expectedSlugName, slugName)
    }

    @Test
    fun create_slug_by_name_with_dash() {
        val name = "São-Paulo-SP-Tiete"
        val expectedSlugName = "sao-paulo-sp-tiete"
        val slugName = toSlug(name)
        Assert.assertEquals(expectedSlugName, slugName)
    }

    @Test
    fun create_slug_by_name_with_whitespace() {
        val name = "São Paulo SP Tiete"
        val expectedSlugName = "sao-paulo-sp-tiete"
        val slugName = toSlug(name)
        Assert.assertEquals(expectedSlugName, slugName)
    }

    @Test
    fun create_slug_by_name_with_two_whitespace() {
        val name = "São Paulo SP  Tiete"
        val expectedSlugName = "sao-paulo-sp-tiete"
        val slugName = toSlug(name)
        Assert.assertEquals(expectedSlugName, slugName)
    }



}
