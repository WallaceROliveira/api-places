package br.com.queroserclickbus.apiplaces.fixture

import br.com.queroserclickbus.apiplaces.dto.PlaceDTO
import br.com.queroserclickbus.apiplaces.model.Place
import br.com.six2six.fixturefactory.Fixture
import br.com.six2six.fixturefactory.Rule
import br.com.six2six.fixturefactory.loader.TemplateLoader
import java.util.*

class PlaceFixture : TemplateLoader {

    override fun load() {
        Fixture.of(PlaceDTO::class.java).apply {
            addTemplate("validPlaceDTO",
                    Rule().apply {
                        add("id", random(Long::class.java, range(1L, 200L)))
                        add("name", "JD Paulista")
                        add("city", "São Paulo")
                        add("slug", "jd-paulista-sao-paulo")
                        add("createdAt", Date())
                        add("updatedAt", null)
                    })
        }

        Fixture.of(PlaceDTO::class.java).apply {
            addTemplate("invalidNamePlaceDTO",
                    Rule().apply {
                        add("name", "")
                        add("city", "Rio de janeiro")
                    })
        }

        Fixture.of(PlaceDTO::class.java).apply {
            addTemplate("invalidCityPlaceDTO",
                    Rule().apply {
                        add("name", "Tiete")
                        add("city", "")
                    })
        }

        Fixture.of(Place::class.java).apply {
            addTemplate("validPlace",
                    Rule().apply {
                        add("id", random(Long::class.java, range(1L, 200L)))
                        add("name", "JD Paulista")
                        add("city", "São Paulo")
                        add("slug", "jd-paulista-sao-paulo")
                        add("createdAt", Date())
                        add("updatedAt", null)
                    })
        }

        Fixture.of(Place::class.java).apply {
            addTemplate("validPlaceWithoutSlug",
                    Rule().apply {
                        add("name", "JD Paulista")
                        add("city", "São Paulo")
                        add("createdAt", Date())
                        add("updatedAt", null)
                    })
        }


    }

}
