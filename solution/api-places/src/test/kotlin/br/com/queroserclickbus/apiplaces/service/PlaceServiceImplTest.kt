package br.com.queroserclickbus.apiplaces.service

import br.com.queroserclickbus.apiplaces.dto.PlaceDTO
import br.com.queroserclickbus.apiplaces.model.Place
import br.com.queroserclickbus.apiplaces.repository.PlaceRepository
import br.com.six2six.fixturefactory.Fixture
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader
import junit.framework.Assert.assertEquals
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner
import org.modelmapper.ModelMapper
import javax.persistence.NoResultException

@RunWith(MockitoJUnitRunner::class)
class PlaceServiceImplTest {

    @InjectMocks
    lateinit var placeServiceImpl: PlaceServiceImpl

    @Mock
    lateinit var placeRepository: PlaceRepository

    @Mock
    lateinit var modelMapper: ModelMapper

    companion object {
        @BeforeClass
        @JvmStatic
        fun setUp() =
                FixtureFactoryLoader.loadTemplates("br.com.queroserclickbus.apiplaces.fixture")
    }


    @Test
    fun find_place_by_slug() {

        val place = Fixture.from(Place::class.java).gimme<Place>("validPlace")
        var placeDTO = Fixture.from(PlaceDTO::class.java).gimme<PlaceDTO>("validPlaceDTO")
        val slug = "jd-paulista-sao-paulo"

        `when`(placeRepository.findBySlug(slug)).thenReturn(place)
        `when`(modelMapper.map(place, PlaceDTO::class.java)).thenReturn(placeDTO)

        val placeDTO2 = placeServiceImpl.findBySlug(slug)

        assertEquals(slug, placeDTO2.slug)

    }

    @Test(expected = NoResultException::class)
    @Throws(NoResultException::class)
    fun not_found_place_by_slug_throw_no_result_exception() {

        val slug = "jd-paulista-sao-paulo2"

        `when`(placeRepository.findBySlug(slug)).thenReturn(null)

        val placeDTO2 = placeServiceImpl.findBySlug(slug)

    }

}

